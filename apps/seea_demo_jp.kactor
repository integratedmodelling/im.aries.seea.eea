app seea.demo.jp
	"SEEA Ecosystem Accounting Explorer"
	description "このアプリケーションは、ARIESプラットフォームにSEEA環境会計標準を実装します." 
	logo images/un_logo.png
	style dark

@left
action main:
	%%%
		SEEA実験生態系アカウント（**SEEA-EEA**）は、
		生態系とそれらが経済に提供するサービスの範囲と状態、両方で測定
		生物物理学的および金銭的用語。

		ARIES SEEA-EEAツールボックスは、
		グローバルデータとモデルだけでなく、ローカルデータとモデルを使用したアカウントのカスタマイズ
		パラメータ化。ツールボックスを使用するには、コンテキスト（場所と年）とアカウントを選択します
		興味。 ARIESは、コンテキストに最適な利用可能なデータを使用してアカウントを作成します。
		
	%%% :height 200 :collapse
	
	/**
	 * Main choice of accounts
	 */
	(
		(
			tree(:check width=240 "Extent accounts" 
				<- {type of ecology.incubation:Ecosystem}
				<- {landcover:LandCoverType}
		    ): chooseextent($)
		  %%%
			   *エクステントアカウントは、面積カバレッジに関する情報を整理します
			    国または関心のある他の地域内の生態系タイプの
			   （つまり、生態系会計領域）。*
		   %%% :width 280 :height 140 :top
		) :hbox :hfill :name "Extent accounts"
		
	    (tree(:check height=320 width=240 "Condition accounts"
			<- ({landcover:Forest}
				 <- "Leaf area index"
				 <- {ecology:NormalizedDifferenceVegetationIndex}
				 <- {im:Critical conservation:Pristine ecology:Forest earth:Region})
				//Burned area, drought - still need semantics
			<- {landcover:Grassland}
			<- {landcover:Shrubland}
			<- {landcover:Wetland}
		 ): choosecondition($)

		   %%%
			   *生態系の状態のアカウントは、測定する一連の主要な指標を提供します
                               生態系資産の全体的な品質、その状態または機能、
                               自然、そして生態系サービスを供給する可能性*
		   %%% :width 280 :height 140 :top		) :hbox :hfill :name "Condition accounts"

		(tree(:check width=240 "Physical supply-use accounts"
			<- {ecology:Vegetation chemistry:Carbon im:Mass in t/ha}
			<- {im:Net value of ecology:Pollination}
			<- {value of es:FloodRegulation}
			<- {value of `behavior:Outdoor `behavior:Recreation 0 to 1}
		 ): choosephysical($)
		  %%%
			   *物理的な供給使用アカウントは、生態系サービスの供給を測定します
                                経済単位で分類された、対応する受益者。価値観
                                年間の物理的な条件です*
		  %%% :width 280 :height 140 :top
		) :hbox :hfill :name "Physical supply-use accounts"
		 	
		(tree(:check width=240 "Monetary supply-use accounts"
			<- {ecology:Vegetation chemistry:Carbon im:Mass in t/ha}
			<- {im:Net value of ecology:Pollination}
			<- {value of es:FloodRegulation}
			<- {value of `behavior:Outdoor `behavior:Recreation 0 to 1}
		 ): choosemonetary($)
		  %%%
			   *貨幣供給使用勘定は、生態系サービスの供給を測定します
                               それに対応する受益者は、経済単位で分類されます。価値観
                               年間の金額で表示されます。*
		  %%% :width 280 :height 140 :top 
		) :hbox :hfill :name "Monetary supply-use accounts"
		 
		(tree(:check width=240 "Asset accounts"
			<- {ecology:Vegetation chemistry:Carbon im:Mass in t/ha}
			<- {im:Net value of ecology:Pollination}
			<- {value of es:FloodRegulation}
			<- {value of `behavior:Outdoor `behavior:Recreation 0 to 1}
		 ): chooseasset($)
		 %%%
			   *資産勘定はすべての生態系資産の金銭的価値を記録します
                                生態系会計領域内およびそれらの在庫の変化
                                時間、正味現在価値として計算されます。*
		 %%% :width 280 :height 140 :top
		) :hbox :hfill :name "Asset accounts"
		 
	) :shelf :hfill
	%%%
		
		1つ以上の集約基準を選択すると、特派員が作成されます
		このコンテキストのアカウンティングレポートのテーブル。
	
	%%%
	(checkbutton("National boundaries        ")
	 checkbutton("First-level administrative division")
	 checkbutton("Watershed")) :name "Aggregate by" :hfill
